'use strict';

import { Bucket, File, MakeFilePublicResponse } from '@google-cloud/storage';
import { BeCLog, BeCLogger, deserialize, serialize, StorageItem, StorageMetadata } from '@vid/core';
import * as internalClient from 'deployd/lib/internal-client.js';
import * as Resource from 'deployd/lib/resource.js';
import * as formidable from 'formidable';
import { Fields, Files } from 'formidable';
import * as fs from 'fs';
import { clone } from 'lodash';
import * as path from 'path';

export interface Config {
  dbPathPrefixes: {
    contents?: string, devices?: string, deviceinterfaces?: string, i18n?: string, pages?: string, projects?: string, stations?: string, files?: string, templates?: string, users?: string, versions?: string, versiondescriptors?: string
  }
  mandant: string;
}

export interface Context {
  debug: boolean;
  url: any;
  req: any;
  res: any;
  body: any;
  query: any;
  server: any;
  session: any;
  method: any;

  done: ( err, res? ) => {};
  end: ( err, res? ) => {};
}

let env;
if ( (process as any).server && (process as any).server.options && (process as any).server.options.env ) {
  env = (process as any).server.options.env;
} else {
  env = null;
}

let publicDir = '../../../../public';

class GcloudUpload extends Resource {

  public logger: BeCLogger = BeCLog.createClogger( 'vid:dpd-gcloud-upload', 'GcloudUpload' );

  public config;

  public store;

  public name;

  public session;

  public dpd: any;

  public static label = 'UploadGCLOUD';

  public static events = [ 'upload', 'delete' ];

  public events;

  public static clientGeneration = true;

  public clientGeneration = true;

  public static basicDashboard = {
    settings: [
      {
        name: 'keyFilename',
        type: 'text',
        description: 'Full path (relative to "public") to the .json key, downloaded from the Google Developers Console'
      }, {
        name: 'bucket',
        type: 'text',
        description: 'gcloud bucket name'
      }, {
        name: 'mandant',
        type: 'text',
        description: 'The Mandant f.E. "henkel"'
      }, {
        name: 'storeName',
        type: 'text',
        description: 'The storeName f.E. "henkel/cms/files"'
      }, {
        name: 'authorization',
        type: 'checkbox',
        description: 'Do you require user to be logged-in to upload files  Defaults to \'false\' ?'
      }, {
        name: 'uniqueFilename',
        type: 'checkbox',
        description: 'Allow unique file name Defaults to \'true\' ?'
      }, {
        name: 'makePublic',
        type: 'checkbox',
        description: 'Make uploaded File public Defaults to \'true\' ?'
      }, {
        name: 'generateQRCode',
        type: 'checkbox',
        description: 'Generate QR Code of URL Defaults to \'true\' ?'
      }, {
        name: 'directory',
        type: 'text',
        description: 'Directory where file is stored (relative to "public")'
      }
    ]
  };

  constructor( options ) {
    super( options );

    Resource.apply( this, arguments );

    if ( env ) {
      const dirToCheck = publicDir + '-' + env, publicDirExists = fs.existsSync( __dirname + dirToCheck );
      if ( publicDirExists ) {
        publicDir = dirToCheck;
      }
    }

    this.config = {
      keyFilename: this.config.keyFilename,
      bucket: this.config.bucket,
      mandant: this.config.mandant,
      storeName: this.config.storeName || 'cms/files',
      directory: this.config.directory || 'upload',
      fullDirectory: path.join( __dirname, publicDir, this.config.directory || 'upload' ),
      authorization: this.config.authorization || false,
      makePublic: this.config.makePublic || true,
      generateQRCode: this.config.authorization || true,
      uniqueFilename: this.config.uniqueFilename || false
    };

    if ( this.name === this.config.directory ) {
      this.config.directory = this.config.directory + '_';
    }

    // If the directory doesn't exists, we'll create it
    try {
      fs.statSync( this.config.fullDirectory )
        .isDirectory();
    } catch ( er ) {
      fs.mkdir( this.config.fullDirectory, e => this.logger.error( 'mkdir error %O', e ) );
    }

    // this.store = (process as any).server.createStore( this.name + 'fileupload' );
    this.store = (process as any).server.createStore( this.config.storeName );

  }

  /**
   * Module methods
   */
  public handle( ctx: Context, next ) {
    // session um einen client zu erzeugen
    if ( !ctx.debug ) {
      if ( ctx.session ) {
        this.dpd = internalClient.build( (process as any).server, ctx.session, null, ctx );
      } else {
        this.dpd = internalClient.build( (process as any).server, null, null, ctx );
      }
    }

    const req = ctx.req;
    const self = this;
    const domain: { url: any; data?: any } = { url: ctx.url };
    const me = ctx.session.user;

    let uploaderId = '';

    if ( this.config.authorization && !me ) {
      return ctx.done( {
                         statusCode: 403,
                         message: 'You\'re not authorized to upload / modify files.'
                       } );
    }

    const keyFilename = path.join( __dirname, publicDir, this.config.keyFilename );

    const { Storage } = require( '@google-cloud/storage' );
    const bucket: Bucket = new Storage( {
                                          keyFilename: keyFilename
                                        } )
      .bucket( this.config.bucket );

    let subdir;

    if ( typeof req.query !== 'undefined' ) {
      subdir = req.query.subdir;
    }

    let fileName;

    if ( typeof req.query !== 'undefined' ) {
      fileName = req.query.fileName;
    }

    if ( me ) {
      uploaderId = me.id;
    }

    const mandant = this.config.mandant;

    let remainingFile = 0;
    const uniqueFilename = this.config.uniqueFilename;

    const results = [];

    /**
     {
      "kind": "storage#object",
      "id": "fidlock/fid_webapp/2018111381528-03_uni-pet_bb_ur_rr_start_v01_00000.jpg/1542096928457161",
      "selfLink": "https://www.googleapis.com/storage/v1/b/fidlock/o/fid_webapp%2F2018111381528-03_uni-pet_bb_ur_rr_start_v01_00000.jpg",
      "name": "fid_webapp/2018111381528-03_uni-pet_bb_ur_rr_start_v01_00000.jpg",
      "bucket": "fidlock",
      "generation": "1542096928457161",
      "metageneration": "1",
      "contentType": "image/jpeg",
      "timeCreated": "2018-11-13T08:15:28.456Z",
      "updated": "2018-11-13T08:15:28.456Z",
      "storageClass": "REGIONAL",
      "timeStorageClassUpdated": "2018-11-13T08:15:28.456Z",
      "size": "157378",
      "md5Hash": "7S/QsHGVurfOd4yBwfZthg==",
      "mediaLink": "https://www.googleapis.com/download/storage/v1/b/fidlock/o/fid_webapp%2F2018111381528-03_uni-pet_bb_ur_rr_start_v01_00000.jpg?generation=1542096928457161&alt=media",
      "contentDisposition": "inline; filename=2018111381528-03_uni-pet_bb_ur_rr_start_v01_00000.jpg",
      "cacheControl": "public, max-age=3600",
      "metadata": {
      "originalFilename": "03_uni-pet_bb_ur_rr_start_v01_00000.jpg"
    },
      "crc32c": "kq+R2A==",
      "etag": "CMmDhd320N4CEAE="
    }
     */
    const getStorageItem = ( file, res ) => {
      const storedObject: StorageItem = deserialize( StorageItem, {} );
      storedObject.createdDate = new Date();
      storedObject.createdBy = uploaderId;
      storedObject.mandant = mandant;
      storedObject.name = file.name;
      storedObject.uploaderResourceName = self.name;
      storedObject.itemUrl = res.bucket + '/' + res.name;

      const storedMeta: StorageMetadata = deserialize( StorageMetadata, res );
      storedMeta.subdir = subdir;
      storedMeta.originalFilename = file.originalFilename;
      storedMeta.size = res.size;
      storedMeta.contentType = res.contentType;

      storedObject.metadata = storedMeta;
      if ( storedObject.id ) delete storedObject.id;

      return storedObject;
    };

    const runUploadEvent = ( result ) => {
      domain.data = result;
      domain[ 'this' ] = result;
      (GcloudUpload.events as any).upload.run( ctx as any, domain, ( err ) => {
        if ( err ) ctx.done( err );
        return ctx.done( null, result );
      } );
    };

    const runDeleteEvent = ( result ) => {
      if ( (GcloudUpload.events as any).delete ) {
        domain.data = result;
        domain[ 'this' ] = result;
        (GcloudUpload.events as any).delete.run( ctx as any, domain, ( err ) => {
          if ( err ) ctx.done( err );
          ctx.done( null, result );
        } );
      }
    };

    // Will send the response if all files have been processed
    const processDone = ( err = void 0 ) => {
      if ( err ) return ctx.done( err );
      remainingFile--;
      if ( remainingFile === 0 ) {
        // this.logger.info('Response sent: ', result);
        if ( (GcloudUpload.events as any).upload ) {
          return runUploadEvent( results );
        } else {
          return ctx.done( null, results );
        }
      }
    };

    if ( req.method === 'POST' || req.method === 'PUT' ) {

      const form = new formidable.IncomingForm();

      form
        .on( 'file', ( name, file ) => {
          this.logger.info( 'File received:' + file.name );
          file.originalFilename = file.name;
          file.pathName = file.name;
          if ( uniqueFilename ) {
            file.pathName = file.name = this.dateNowString() + file.name;
            this.logger.info( 'uniqueFilename:' + file.pathName );
          }
          if ( subdir ) {
            file.pathName = path.join( subdir, file.name );
            this.logger.info( 'subdir:' + file.pathName );
          }
          this.uploadUploadedFile( bucket, file )
              .then( ( uploadRes ) => {
                // this.logger.info( 'uploadUploadedFile uploadRes:', uploadRes );
                const res = uploadRes[ 1 ];
                if ( uniqueFilename ) {
                  res.originalFilename = file.originalFilename;
                }
                if ( this.config.makePublic ) {
                  this.makePublic( bucket, file.pathName )
                      .then( makePublicRes => {
                        // this.logger.info('makePublic makePublicRes:' ,makePublicRes);
                        const storedObject: StorageItem = getStorageItem( file, res );
                        self.store.insert( serialize( storedObject ), ( err, insertResult ) => {
                          if ( err ) {
                            this.logger.error( 'store.insert failed', err );
                            return processDone( err );
                          }
                          // this.logger.info( 'stored after event.upload.run %j', err || insertResult || 'none' );
                          const cloneResult = clone( insertResult );
                          results.push( cloneResult );
                          processDone( null );
                        } );
                      } )
                      .catch( err => {
                        this.logger.error( 'makePublic failed', err );
                        processDone( err );
                      } );
                } else {
                  processDone( null );
                }
              } )
              .catch( err => {
                this.logger.error( 'uploadUploadedFile failed', err );
                processDone( err );
              } );

        } )
        .on( 'fileBegin', ( name, file ) => {
          remainingFile++;
          // this.logger.info('Receiving a file: %j', file.name);
        } )
        .on( 'error', ( err ) => {
          // this.logger.info('Error: %j', err);
          return processDone( err );
        } );

      form.parse( req, ( err: any, fields: Fields, files: Files ) => {
        if ( err ) this.logger.error( 'error:', err );
      } );

      return req.resume();

    } else if ( req.method === 'GET' ) {

      if ( typeof fileName !== 'undefined' && fileName !== null && fileName !== '' ) {

        const filePath = path.join( __dirname, publicDir, this.config.directory, fileName );

        this.uploadFile( bucket, filePath )
            .then( () => {
              this.generateSignedUrl( bucket, fileName )
                  .then( results => {
                    const url = results[ 0 ];
                    this.generateQRCode( url, ( err, png ) => {
                      if ( err ) {
                        // `err` may be a string or Error object
                        console.error( 'ERROR:', err );
                        const reserr = {
                          'Error': '"Error uploading file. generateQRCode',
                          'fileName': fileName,
                          'err': err
                        };
                        ctx.done( null, reserr );
                      } else {
                        // `png` is a Buffer
                        // png.length           : PNG file length
                        // png.readUInt32BE(16) : PNG image width
                        // png.readUInt32BE(20) : PNG image height

                        const result = {
                          'result': 'Successfully uploaded file',
                          'fileName': fileName,
                          'url': url,
                          'qrcodeWidth': png.readUInt32BE( 16 ),
                          'qrcodeHeight': png.readUInt32BE( 20 ),
                          'qrcode': 'data:image/png;base64,' + (png as any).toString( 'base64' )
                        };
                        if ( (GcloudUpload.events as any).upload ) {
                          runUploadEvent( result );
                        } else {
                          ctx.done( null, result );
                        }
                      }
                    } );

                  } )
                  .catch( err => {
                    console.error( 'ERROR:', err );
                    const reserr = {
                      'Error': '"Error uploading file. generateSignedUrl',
                      'fileName': fileName,
                      'err': err
                    };
                    ctx.done( null, reserr );

                  } );
            } )
            .catch( err => {
              console.error( 'ERROR:', err );
              const reserr = {
                'Error': '"Error uploading file. uploadFile',
                'fileName': fileName,
                'err': err
              };
              ctx.done( null, reserr );
            } );
      } else {
        ctx.done( null, { 'error': 'no fileName' } );
      }

    } else if ( req.method === 'DELETE' ) {

      if ( fileName ) {
        this.deleteFile( bucket, fileName )
            .then( deleteRes => {
              // this.logger.info('deleteFile deleteRes:' + JSON.stringify(deleteRes));
              if ( (GcloudUpload.events as any).delete ) {
                return runDeleteEvent( deleteRes );
              } else {
                return ctx.done( null, deleteRes );
              }
            } )
            .catch( err => ctx.done( err ) );
      } else {
        ctx.done( null, { 'error': 'no fileName ' } );
      }
    }
  }

  private uploadUploadedFile( bucket: Bucket, uploadedFile ) {
    const destination = bucket.file( uploadedFile.pathName );
    const options = {
      destination: destination,
      resumable: true,
      metadata: {
        originalFilename: uploadedFile.originalFilename,
        contentType: uploadedFile.type,
        cacheControl: 'public, max-age=3600',
        contentDisposition: 'inline; filename=' + uploadedFile.name,
        metadata: {
          originalFilename: uploadedFile.originalFilename
        }
      }
    };
    return bucket.upload( uploadedFile.path, options );
  }

  private uploadFile( bucket: Bucket, fileName ) {
    // Uploads a local file to the bucket
    return bucket.upload( fileName );
  }

  private deleteFile( bucket: Bucket, fileName ) {
    // deletes file from bucket
    return bucket.file( fileName )
                 .delete();
  }

  private makePublic( bucket: Bucket, fileName ): Promise<MakeFilePublicResponse> {
    // Makes the file public
    return (bucket.file( fileName ) as File).makePublic();
  }

  private generateSignedUrl( bucket: Bucket, fileName ) {
    // These options will allow temporary read access to the file
    const options = {
      action: 'read',
      expires: '03-17-2099'
    } as any;
    // Get a signed URL for the file
    return bucket.file( fileName )
                 .getSignedUrl( options );
  }

  private generateQRCode( url, handler ) {

    const bwipjs = require( 'bwip-js' );

    bwipjs.toBuffer( {
                       bcid: 'qrcode',       // Barcode type
                       text: url,    // Text to encode
                       scale: 2,               // scaling factor
                       includetext: false,            // Show human-readable text
                       textxalign: 'center'        // Always good to set this
                     }, handler );

  }

  private dateNowString() {
    const now = new Date();
    return '' + now.getUTCFullYear() + '' + (now.getUTCMonth() + 1) + '' + now.getUTCDate() + '' + now.getUTCHours() + '' + now.getUTCMinutes() + '' + now.getUTCSeconds() + '-';
  }
}

/**
 * Module export
 */
module.exports = GcloudUpload;
